export class EferiaActor extends Actor {
  prepareData() {
    super.prepareData();
  }

  prepareBaseData() {
  }

  prepareDerivedData() {
    const actorData = this;
    const systemData = actorData.system;
    const flags = actorData.flags.eferia;
    
    this._prepareCharacterData(actorData);
    this._prepareEnemyData(actorData);

    return data;
  }

  _prepareCharacterData(actorData) {
    if (actorData.type !== 'character') return;

    const systemData = actorData.system;

    


