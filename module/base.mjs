import { EferiaActor } from "./documments/actor.mjs";
import { EferiaItem } from "./documments/item.mjs";

import { EferiaActorSheet } from "./documments/actor.mjs";
import { EferiaItemSheet } from "./documments/item.mjs";

import { preloadHandlebarsTemplate } from "./helpers/templates.mjs";
import { EFERIA } from ".helpers/config.mjs";

Hooks.once('init', function() {
  game.customgame = {
    EferiaActor,
    EferiaItem
  }

  CONFIG.EFERIA = EFERIA

  CONFIG.Actor.documentClass = EferiaActor;
  CONFIG.Item.documentClass = EferiaItem;

  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("eferia", EferiaActorSheet, { makeDefault: true });

  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("eferia", EferiaItemSheet, { makeDefault: true });

  return preloadHandlebarsTemplates();
});

Hooks.once('ready', function() {})
